Name:            backintime
Version:         1.1.10
Release:         0%{?dist}
Summary:         Simple backup tool inspired from the Flyback project and TimeVault
License:         GPLv2+
URL:             http://backintime.le-web.org
Source0:         https://github.com/bit-team/%{name}/releases/download/v1.1.10/%{name}-%{version}.tar.gz
BuildArch:       noarch
BuildRequires:   desktop-file-utils
BuildRequires:   gettext
BuildRequires:   python3-devel
Requires:        python3-SecretStorage
Requires:        openssh-clients
Requires:        python3-keyring
Requires:        python3-dbus
Requires:        fuse-sshfs
Requires:        fuse-encfs

%description
Back In Time is a simple backup system for Linux inspired from 
“flyback project” and “TimeVault”. The backup is done by taking 
snapshots of a specified set of directories.


%package         qt4
Summary:         Qt4 frontend for %{name}
Requires:        %{name} = %{version}-%{release}
Requires:        python3-PyQt4
Requires:        xorg-x11-utils
Requires:        libnotify
Requires:        polkit
Requires:        python3-SecretStorage
Requires:        python3-keyring

%description     qt4
BackInTime is a simple backup system for Linux inspired from 
“flyback project” and “TimeVault”. The backup is done by taking 
snapshots of a specified set of directories.

This package contains the Qt4 frontend of BackInTime.

%prep
%autosetup

%build
pushd common
./configure --no-fuse-group --python3
make %{?_smp_mflags}
popd

pushd qt4
./configure --python3
make %{?_smp_mflags}
popd

%install

# Force Python 3 to be used for byte compilation:
%global __python %{__python3}

pushd common
make install \
     INSTALL="install -p" \
     DESTDIR="%{buildroot}"
popd

pushd qt4
make install \
     INSTALL="install -p" \
     DESTDIR="%{buildroot}"
popd

mkdir -p %{buildroot}%{_sbindir}
cp -p %{buildroot}%{_bindir}/%{name}-qt4 \
      %{buildroot}%{_sbindir}/%{name}-qt4-root

ln -s consolehelper \
      %{buildroot}%{_bindir}/%{name}-qt4-root

mkdir -p %{buildroot}%{_sysconfdir}/security/console.apps/

cat << EOF > %{buildroot}%{_sysconfdir}/security/console.apps/%{name}-qt4-root
USER=root
PROGRAM=%{_sbindir}/%{name}-qt4-root
SESSION=true
EOF

mkdir -p %{buildroot}%{_sysconfdir}/pam.d

cat << EOF > %{buildroot}%{_sysconfdir}/pam.d/%{name}-qt4-root
#%PAM-1.0
auth            include         config-util
account         include         config-util
session         include         config-util
EOF

%find_lang %{name}

%files -f %{name}.lang
%doc %{_docdir}/%{name}-common/
%{_sysconfdir}/xdg/autostart/%{name}.desktop
%{_bindir}/%{name}
%{_bindir}/%{name}-askpass
%dir %{_datadir}/%{name}/
%{_datadir}/%{name}/common/
%dir %{_datadir}/%{name}/plugins/
%{_datadir}/%{name}/plugins/user*plugin*
%{_datadir}/%{name}/plugins/notify*plugin*
%{_datadir}/%{name}/plugins/__pycache__/user*plugin*
%{_datadir}/bash-completion/completions/backintime
%{_datadir}/dbus-1/system-services/net.launchpad.backintime.serviceHelper.service
%{_datadir}/polkit-1/actions/net.launchpad.backintime.policy
%{_mandir}/man1/%{name}*
%{_sysconfdir}/dbus-1/system.d/net.launchpad.backintime.serviceHelper.conf


%files qt4
%doc %{_docdir}/%{name}-qt4/
%{_bindir}/%{name}-qt4
%{_bindir}/%{name}-qt4-root
%{_sbindir}/%{name}-qt4-root
%{_datadir}/applications/%{name}-qt4.desktop
%{_datadir}/applications/%{name}-qt4-root.desktop
%{_datadir}/backintime/qt4/
%{_datadir}/%{name}/plugins/qt4plugin*
%{_datadir}/%{name}/plugins/__pycache__/qt4*plugin*
%{_datadir}/%{name}/plugins/__pycache__/notify*plugin*
%{_datadir}/doc/qt/HTML/en/backintime/index.docbook
%{_datadir}/icons/hicolor/*/actions/*.*
%config(noreplace) %{_sysconfdir}/pam.d/%{name}-qt4-root
%config %{_sysconfdir}/security/console.apps/%{name}-qt4-root


%changelog
* Sun Jan 10 2016 Martin Hoeher <martin@rpdev.net> - 1.1.10-0
- Updated to 1.1.10
- Removed dependency to pm-utils (does not longer exist in Fedora)

* Mon Dec 21 2015 Martin Hoeher <martin@rpdev.net> - 1.1.8-0
- Update to 1.1.8

* Wed Aug 12 2015 Martin Hoeher <martin@rpdev.net> - 1.1.6-1
- Update to 1.1.6, restructured to match new BiT structure (split into common and qt4)

* Tue Aug 12 2014 Christopher Meng <rpm@cicku.me> - 1.0.36-1
- Update to 1.0.36

* Sat Jun 07 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.34-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Sun Dec 22 2013 Christopher Meng <rpm@cicku.me> - 1.0.34-1
- Update to 1.0.34(BZ#1043054)
- Switch to kdesu(BZ#1038893)

* Sun Nov 24 2013 Christopher Meng <rpm@cicku.me> - 1.0.28-2
- Enable notify plugin, separate plugins into different subpackages.
- Add missing python-SecretStorage dependency(BZ#1031403).
- Remove fuse group check(BZ#1010728).
- python-keyring incompatibility fix(BZ#1026542).

* Mon Oct 21 2013 Christopher Meng <rpm@cicku.me> - 1.0.28-1
- Update to 1.0.28(BZ#1021192) with fixes(BZ#1014293),(BZ#1014976).

* Thu Sep 12 2013 Christopher Meng <rpm@cicku.me> - 1.0.26-2
- Add missing python-keyring dependency(BZ#1007315).

* Wed Sep 11 2013 Christopher Meng <rpm@cicku.me> - 1.0.26-1
- Update to 1.0.26(BZ#1003304),(BZ#951248).
- Fix Bugs(BZ#999935),(BZ#922534).

* Sat Aug 03 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.8-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Wed Feb 13 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.8-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_19_Mass_Rebuild

* Wed Jul 18 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.8-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Thu Jan 12 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.8-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Sun Oct 09 2011 Tim Jackson <rpm@timj.co.uk> - 1.0.8-2
- Add missing dependency on gnome-python2-gnome (rhbz#720577)

* Sun Oct 09 2011 Tim Jackson <rpm@timj.co.uk> - 1.0.8-1
- Update to version 1.0.8

* Mon Feb 07 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.6-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Thu Feb 03 2011 Tim Jackson <rpm@timj.co.uk> - 1.0.6-3
- Fix bad ownership of language files (RHBZ #569407)

* Wed Feb 02 2011 Tim Jackson <rpm@timj.co.uk> - 1.0.6-2
- Fix error if notify-python is not installed (RHBZ #630969)

* Wed Feb 02 2011 Tim Jackson <rpm@timj.co.uk> - 1.0.6-1
- Update to version 1.0.6

* Wed Aug 11 2010 David Malcolm <dmalcolm@redhat.com> - 0.9.26-4
- recompiling .py files against Python 2.7 (rhbz#623275)

* Wed Sep 02 2009 Simon Wesp <cassmodiah@fedoraproject.org> - 0.9.26-3
- Add patch0 to secure backups

-* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.9.26-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Sat May 23 2009 Simon Wesp <cassmodiah@fedoraproject.org> - 0.9.26-1
- New upstream release
- Drop 'removecheck'-patch

* Sun May 17 2009 Simon Wesp <cassmodiah@fedoraproject.org> - 0.9.24-3
- Fix sammries, RHBZ #501085

* Tue May 12 2009 Simon Wesp <cassmodiah@fedoraproject.org> - 0.9.24-2
- fix doc issues, LP #375113

* Thu May 07 2009 Simon Wesp <cassmodiah@fedoraproject.org> - 0.9.24-1
- New upstream release

* Sat Apr 25 2009 Simon Wesp <cassmodiah@fedoraproject.org> - 0.9.22-2
- Remove Patch for desktop-files and do the changes in spec-file
- Change description of gnome package to "Gnome frontend for NAME" 
- Change description of kde package to "KDE frontend for NAME"
- Add TRANSLATIONS to DOC of common package
- Mark _DATADIR/gnome/help/NAME as DOC
- Mark _DATADIR/doc/kde4/HTML/en/NAME as DOC
- Use cp -p when copying from bindir to sbindir

* Wed Apr 22 2009 Simon Wesp <cassmodiah@fedoraproject.org> - 0.9.22-1
- New upstream release
- Add Patch to remove the Desktopchecks in configure

* Mon Apr 06 2009 Simon Wesp <cassmodiah@fedoraproject.org> - 0.9.20-1
- New upstream release
- Add consolehelperstuff for root-access

* Tue Mar 17 2009 Simon Wesp <cassmodiah@fedoraproject.org> - 0.9.16.1-1
- New upstream release

* Tue Mar 10 2009 Simon Wesp <cassmodiah@fedoraproject.org> - 0.9.14-1
- Initial Package build
